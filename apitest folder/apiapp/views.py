from django.shortcuts import render
from . import models
from django.http import HttpResponse,HttpResponseRedirect,JsonResponse
from . import db
import csv
# Create your views here.
def save_lat_long(request):
    pin_code=request.POST.get('pincode')
    lat=request.POST.get('latitude')
    longi=request.POST.get('longitude')
    place_name=request.POST.get('placename')
    admin_name=request.POST.get('adminname')
    accuracy=request.POST.get('Accuracy')
    
    pin=db.check_pincode(pin_code)

    if pin:
        return JsonResponse({"status":"0","message":"PIn code already EXIST"},status=400)
    else:
        p=db.lat_long(pin_code,lat,longi,place_name,admin_name,accuracy)
        if p:
            return JsonResponse({"status":"1","message":'pin code saved successfully',},status=200)



def get_all_lat_long(request):
    radius=request.POST.get('radius')
    LAT=request.POST.get('LAT')
    LON=request.POST.get('LON')
    dist=db.getpoints(radius,LAT,LON)
    if(dist):
        return JsonResponse({"status":"1","message":dist,},status=200)
    else:
        return JsonResponse({"status":"0","message":'no place found',},status=400)

def load_json_data():
    with open('C:/Users/ramneek/Desktop/map.geojson') as file: 
        data = file.read()

    geo=db.store_data(data)
    if(geo):
        return JsonResponse({"status":"1","message":'data saved'},status=200)
    else:
        return JsonResponse({"status":"0","message":'no data saved',},status=400)

def load_csv(request):
    data = csv.DictReader(open("C:/Users/ramneek/Desktop/IN.csv"))
    if data:
        for row in data:
            loc=models.Location.objects.save_location(row['key'],row['place_name'],row['admin_name1'],row['latitude'],row['longitude'],row['accuracy'])
            loc.save()
        return JsonResponse({"status":"1","message":'data   saved',},status=200)
        

    else:
        return JsonResponse({"status":"0","message":'data not  saved',},status=400)
