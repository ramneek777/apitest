from django.contrib import admin
from django.urls import path,include
from . import views
urlpatterns = [
    path('post_location/',views.save_lat_long,name='post_location'),
    path('get_using_self/',views.get_all_lat_long,name='get_using_self')
]