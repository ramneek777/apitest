from . import models

import math


def check_pincode(pin_code):
    pin_c=models.Location.objects.filter(pin_code=pin_code).values()
    if pin_c:
        return True
    else:
        return False


def lat_long(pin_code,lat,longi,place_name,admin_name,accuracy):
    try:
        save_lat=models.Location.objects.save_location(pin_code,lat,longi,place_name,admin_name,accuracy)
        save_lat.save()
        return save_lat
    except Exception as exp:
        print(exp)


def getpoints(radius,LAT,LON):
    l=[]
    try:
        save_lat=models.Location.objects.all()
        for data in save_lat:
            lat=data.latitude
            lon=data.longitude
            dist= 3956 * 2 * math.asin(math.sqrt(math.pow(math.sin((LAT - lat) * 0.0174532925 / 2), 2) + math.cos(LAT * 0.0174532925) * math.cos(lat * 0.0174532925) * math.pow(math.sin((LON - lon) * 0.0174532925 / 2), 2) )) 
            dist=dist/1000
            if dist<radius:
                l.append(save_lat)
        return l
    except Exception as exp:
        print(exp)


def store_data(data):
    try:
        dat=models.geo_location.objects.save_geolocation(data)
        dat.save()
        return dat
    except Exception as exp:
        print(exp)
