from django.db import models
from django.contrib.auth.models import User
from . import managers
from django.contrib.postgres.fields import JSONField
import csv
# Create your models here.

class Location(models.Model):
    pin_code = models.CharField(max_length=100, primary_key=True)
    place_name = models.CharField(max_length=100)
    admin_name = models.CharField(max_length=100)
    latitude = models.DecimalField(max_digits=40,decimal_places=10)
    longitude = models.DecimalField(max_digits=40,decimal_places=10)
    Accuracy=models.IntegerField(default=0)

    objects = managers.Location_manager()
	

class geo_location(models.Model):
    loc=JSONField()
    objects = managers.geo_Location_manager()

