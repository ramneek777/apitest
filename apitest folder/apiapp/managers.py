from django import forms
from django.db import models




class Location_manager(models.Manager):
    def save_location(self,pin_code,place_name,admin_name,longitude,latitude,Accuracy):
    	location_data = self.create(pin_code=pin_code,place_name=place_name,admin_name=admin_name,longitude=longitude,latitude=latitude,Accuracy=Accuracy)
    	return location_data


class geo_Location_manager(models.Manager):
    def save_geolocation(self,loc):
    	loc_data = self.create(loc=loc)
    	return loc_data
